package com.mushynskyi.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC1b3a25e13e6ddbb67e205323708272ca";
  public static final String AUTH_TOKEN = "8fefda694d135086c6e5b930c9e46610";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
            .creator(new PhoneNumber("+380636912150"), /*my phone number*/
                    new PhoneNumber("+12602251060"), str).create(); /*attached to me number*/
  }
}
